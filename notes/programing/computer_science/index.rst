i================
Computer Science
================
***************
Design Patterns
***************
* Design Patterns "Gang of Four"
* Head First

**********
Algorithms
**********
* `Introduction to Algorithms Third Edition <https://mitpress.mit.edu/books/introduction-algorithms-third-edition>`_ [`PDF <file/Introduction_to_Algorithms.pdf>`__]
   The definitive algorithms book. A.K.A. CLRS
* `Algorithms Fourth Edition <https://algs4.cs.princeton.edu/home/>`_ [`GitHub <https://github.com/kevin-wayne/algs4>`__] [`O`Reily <https://www.oreilly.com/library/view/algorithms-fourth-edition/9780132762564/>`__]
   More approachable introduction to algorithms
* `Grokking Algorithms <https://www.manning.com/books/grokking-algorithms>`_
  [O`Reilly `Book <https://learning.oreilly.com/library/view/grokking-algorithms-an/9781617292231/>`__ `Video <https://www.oreilly.com/library/view/grokking-algorithms-video/9781617292231VE/>`__]
  [`PDF <file/Grokking_Algorithms.pdf>`__]
* `Khan Academy: Algorithms <https://www.khanacademy.org/computing/computer-science/algorithms>`_

