======
Python
======

* `Comprehensive Python Cheatsheet <https://gto76.github.io/python-cheatsheet/>`_

********
Podcasts
********
* Python Bytes
* Talk Python
* Python __init__

******************
Learning Resources
******************

* Fluent Python
* Effective Python
* Think Python - How to Think Like a Computer Scientist

- `Modern Python Standard Library Cookbook <https://www.packtpub.com/application-development/modern-python-standard-library-cookbook>`_
  [`GitHub <https://github.com/PacktPublishing/Modern-Python-Standard-Library-Cookbook>`__]
  [`O'Reilly <https://www.oreilly.com/library/view/modern-python-standard/9781788830829/>`__]
- `Python Testing with pytest <https://pragprog.com/book/bopytest/python-testing-with-pytest>`_
  [`O'Reilly <https://www.oreilly.com/library/view/python-testing-with/9781680502848/>`__]
  [`PDF <file/Python_Testing_with_Pytest.pdf>`__]

Computer Science
================
* `Classic Computer Science Problems in Python <https://www.manning.com/books/classic-computer-science-problems-in-python>`_
* `Data Structures and Algorithms in Python <https://www.wiley.com/en-us/Data+Structures+and+Algorithms+in+Python-p-9781118290279>`_
  [`GitHub <https://github.com/mjwestcott/Goodrich>`__]
  [`O'Reilly <https://www.oreilly.com/library/view/data-structures-and/9781118290279/>`__]
  [`PDF <file/Data_Structures_and_Algorithms_in_Python.pdf>`__]

